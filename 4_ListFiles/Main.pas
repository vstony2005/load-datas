unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Memo.Types,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo;

type
  TForm4 = class(TForm)
    Memo1: TMemo;
    btnList: TButton;
    procedure btnListClick(Sender: TObject);
  private
    procedure UpdateFiles(files: TArray<string>);
  public
  end;

var
  Form4: TForm4;

implementation

{$R *.fmx}

uses
  System.IOUtils, system.Generics.Collections, uThreadFileScanner;

procedure TForm4.btnListClick(Sender: TObject);
var
  path, pattern: string;
  thread: TFileScannerThread;
begin
//  path := 'C:\Users\Public\Documents\Embarcadero\Studio\21.0\Samples\';
  path := 'N:\tony\Programmation\src\delphi\';
  pattern := '.dfm';

  Memo1.Lines.Add(Format('Starting file scan in directory [%s] whith pattern [%s]',
                         [path, pattern]));

  Application.ProcessMessages;

  thread := TFileScannerThread.Create;
  thread.Path := path;
  thread.Pattern := pattern;
  thread.FilesUpdate := procedure(AFiles: TArray<string>)
    begin
      UpdateFiles(AFiles);
    end;
  thread.Start;
end;

procedure TForm4.UpdateFiles(files: TArray<string>);
var
  f: string;
begin
  Memo1.Lines.Add(Format('%d files found', [Length(files)]));
  Memo1.BeginUpdate;
  try
    for f in files do
      Memo1.Lines.Add(f);
  finally
    Memo1.EndUpdate;
  end;
end;

end.
