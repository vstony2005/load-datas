unit uThreadFileScanner;

interface

uses
  System.Classes, System.Generics.Collections, System.SysUtils;

type
  TFileScannerThread = class(TThread)
  private
    FPath: string;
    FPattern: string;
    FMatchingFiles: TArray<string>;
    FFilesUpdate: TProc<TArray<string>>;
    procedure SetPath(const Value: string);
    procedure SetPattern(const Value: string);
    procedure SetMatchingFiles(const Value: TArray<string>);
    procedure SetFilesUpdate(const Value: TProc<TArray<string>>);
  protected
      procedure Execute; override;
  public
    constructor Create;
    property Path: string read FPath write SetPath;
    property Pattern: string read FPattern write SetPattern;
    property MatchingFiles: TArray<string> read FMatchingFiles write SetMatchingFiles;
    property FilesUpdate: TProc<TArray<string>> read FFilesUpdate write SetFilesUpdate;
  end;

function FindFiles(const path, pattern: string): TArray<string>;

implementation

uses
  System.IOUtils, System.Types;

function FindFiles(const path, pattern: string): TArray<string>;
var
  f: string;
  files: TArray<string>;
  matchFiles: TList<string>;
begin
  matchFiles := TList<string>.Create;
  try
    files := TDirectory.GetFiles(path);

    for f in files do
      if f.EndsWith(pattern) then
        matchFiles.AddRange(f);

    files := TDirectory.GetDirectories(path);

    for f in files do
      matchFiles.AddRange(FindFiles(f, pattern));

    Result := matchFiles.ToArray;
  finally
    matchFiles.Free;
  end;
end;

{ TFileScannerThread }

constructor TFileScannerThread.Create;
begin
  inherited Create(True);
  FreeOnTerminate := True;
end;

procedure TFileScannerThread.Execute;
begin
  inherited;
  FMatchingFiles := FindFiles(FPath, FPattern);

  if Assigned(FFilesUpdate) then
    Synchronize(nil,
      procedure
      begin
        FFilesUpdate(MatchingFiles);
      end);
end;

procedure TFileScannerThread.SetFilesUpdate(const Value: TProc<TArray<string>>);
begin
  FFilesUpdate := Value;
end;

procedure TFileScannerThread.SetMatchingFiles(const Value: TArray<string>);
begin
  FMatchingFiles := Value;
end;

procedure TFileScannerThread.SetPath(const Value: string);
begin
  FPath := Value;
end;

procedure TFileScannerThread.SetPattern(const Value: string);
begin
  FPattern := Value;
end;

end.
