unit uLoadWithThread;

interface

uses
  System.Classes;

type
  TProcEndEvent = procedure(Sender: TObject; list: TStrings) of object;
  TProcStepEvent = procedure(Sender: TObject; val: Integer) of object;

  TLoadWithThread = class(TThread)
  private
    FFilename: string;
    FOnEnd: TProcEndEvent;
    FOnStep: TProcStepEvent;
    FList: TStrings;
    FValue: Integer;
    procedure SetFilename(const Value: string);
    procedure SetOnEnd(const Value: TProcEndEvent);
    procedure SetOnStep(const Value: TProcStepEvent);
  protected
    procedure Execute; override;
  public
    property Filename: string read FFilename write SetFilename;
    property OnStep: TProcStepEvent read FOnStep write SetOnStep;
    property OnEnd: TProcEndEvent read FOnEnd write SetOnEnd;
    property List: TStrings read FList;
    property Value: Integer read FValue;
  end;

implementation

uses
  IOUtils, System.SysUtils;

{ uLoadWithThread }

procedure TLoadWithThread.Execute;
var
  arr: TArray<string>;
  i: Integer;
  nb, val: Integer;
begin
{$IFDEF DEBUG}
  NameThreadForDebugging('TLoadWithThread');
{$ENDIF}

  if (not TFile.Exists(Filename)) then
    Exit;

  FList := TStringList.Create;
  arr := TFile.ReadAllLines(FFilename);

  nb := Length(arr);
  FValue := 0;

  for i := Low(arr) to High(arr) do
  begin
    FList.Add(arr[i]);

    if Assigned(FOnStep) then
    begin
      val := i * 100 div nb;
      if (val <> FValue) then
      begin
        FValue := val;
        Synchronize(nil, procedure
          begin
            FOnStep(Self, Value);
          end);
      end;
    end;
  end;

  if Assigned(FOnEnd) then
    Synchronize(nil, procedure
      begin
        FOnEnd(Self, List);
      end);
end;

procedure TLoadWithThread.SetFilename(const Value: string);
begin
  FFilename := Value;
end;

procedure TLoadWithThread.SetOnEnd(const Value: TProcEndEvent);
begin
  FOnEnd := Value;
end;

procedure TLoadWithThread.SetOnStep(const Value: TProcStepEvent);
begin
  FOnStep := Value;
end;

end.
