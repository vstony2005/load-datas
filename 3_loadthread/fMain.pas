unit fMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Data.Bind.Controls, FMX.Layouts, FMX.ListBox, Fmx.Bind.Navigator,
  FMX.StdCtrls, FMX.Controls.Presentation;

type
  TForm3 = class(TForm)
    ProgressBar1: TProgressBar;
    Button1: TButton;
    BindNavigator1: TBindNavigator;
    ListBox1: TListBox;
    OpenDialog1: TOpenDialog;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    procedure LoadList(Sender: TObject; lst: TStrings);
    procedure StepProgress(Sender: TObject; val: Integer);
  public
  end;

var
  Form3: TForm3;

implementation

{$R *.fmx}

uses
  uLoadWithThread;

{ TForm3 }

procedure TForm3.LoadList(Sender: TObject; lst: TStrings);
var
  str: string;
begin
  Label1.Text := 'Loading list';
  ListBox1.Items.BeginUpdate;
  ListBox1.Items.Assign(lst);
  ListBox1.Items.EndUpdate;
  Label1.Text := 'Finished';
end;

procedure TForm3.StepProgress(Sender: TObject; val: Integer);
begin
  ProgressBar1.Value := val;
end;

procedure TForm3.Button1Click(Sender: TObject);
var
  thread: TLoadWithThread;
begin
  if (not OpenDialog1.Execute) then
    Exit;

  ListBox1.Clear;
  ProgressBar1.Value := 0;
  Label1.Text := 'Loading file';

  thread := TLoadWithThread.Create;
  thread.Filename := OpenDialog1.FileName;
  thread.OnStep := StepProgress;
  thread.OnEnd := LoadList;
  thread.Resume;
end;

end.
