unit uLoadDatas;

interface

uses
  System.classes, FireDAC.Comp.Client;

type
  TResultEvent = procedure(Sender: TObject; res: Boolean) of object;

  TLoadDatas = class(TThread)
  private
    FOnResult: TResultEvent;
    FTable: TFDMemTable;
    FFilename: string;
    FResult: Boolean;
    procedure DoResult;
  public
    constructor Create(table: TFDMemTable; const filename: string;
        aOnResult: TResultEvent);
    procedure Execute; override;
  end;

implementation

uses
  IOUtils, System.SysUtils;

{ TLoadDatas }

constructor TLoadDatas.Create(table: TFDMemTable; const filename: string;
    aOnResult: TResultEvent);
begin
  inherited Create;

  FTable := table;
  FFilename := filename;
  FOnResult := AOnResult;
end;

procedure TLoadDatas.DoResult;
begin
  if Assigned(FOnResult) then
    FOnResult(Self, FResult);
end;

procedure TLoadDatas.Execute;
var
  lst: TArray<string>;
  i: Integer;
begin
  inherited;

  FResult := False;

  if Assigned(FTable.FindField('word')) and TFile.Exists(FFilename) then
  begin
    lst := TFile.ReadAllLines(FFilename);

    for i:=0 to Length(lst) do
    begin
      if (lst[i].Length >= 6) or (lst[i].Length <= 12) then
      begin
        FTable.Append;
        FTable.FieldByName('word').AsString;
        FTable.Post;
      end;
    end;

    FResult := True;
  end;

  Synchronize(DoResult);
end;

end.
