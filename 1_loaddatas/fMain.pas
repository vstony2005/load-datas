unit fMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.Controls.Presentation,
  FMX.StdCtrls, Data.Bind.EngExt, FMX.Bind.DBEngExt, System.Rtti,
  System.Bindings.Outputs, FMX.Bind.Editors, Data.Bind.Components,
  Data.Bind.DBScope, FMX.Layouts, FMX.ListBox, FireDAC.Stan.StorageBin;

type
  TForm1 = class(TForm)
    FDMemTable1: TFDMemTable;
    Button1: TButton;
    ListBox1: TListBox;
    DataSource1: TDataSource;
    FDMemTable1word: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    Button2: TButton;
    OpenDialog1: TOpenDialog;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    Layout1: TLayout;
    ProgressBar1: TProgressBar;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    procedure EndProcedure(Sender: TObject; res: Boolean);
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  IOUtils, uLoadDatas;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if (FDMemTable1.Active) then
    FDMemTable1.Close;

  FDMemTable1.ResourceOptions.Persistent := True;
  FDMemTable1.ResourceOptions.PersistentFileName :=
    TPath.Combine(TPath.GetDocumentsPath, 'datas.bin');
  FDMemTable1.Open;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FDMemTable1.Close;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  lst: TArray<string>;
  i: Integer;
begin
  if (OpenDialog1.Execute) then
  begin
    ListBox1.BeginUpdate;
    lst := TFile.ReadAllLines(OpenDialog1.FileName);

    ProgressBar1.Max := Length(lst);
    ProgressBar1.Value := 0;

    for i := 0 to Pred(Length(lst)) do
    begin
      ProgressBar1.Value := i;

      FDMemTable1.Append;
      FDMemTable1.FieldByName('word').AsString := lst[i];
      FDMemTable1.Post;
    end;

    ListBox1.EndUpdate;
    ShowMessage('done');
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  lst: TArray<string>;
  load: TLoadDatas;
begin
  if (OpenDialog1.Execute) then
  begin
    ListBox1.BeginUpdate;
    lst := TFile.ReadAllLines(OpenDialog1.FileName);

    ProgressBar1.Max := Length(lst);
    ProgressBar1.Value := 0;

    load := TLoadDatas.Create(FDMemTable1, OpenDialog1.FileName, EndProcedure);
    load.Execute;
  end;
end;

procedure TForm1.EndProcedure(Sender: TObject; res: Boolean);
begin
  ListBox1.EndUpdate;

  if (res) then
    ShowMessage('done')
  else
    ShowMessage('error');
end;

end.
